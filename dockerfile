FROM centos:7.6.1810

RUN yum install -y SDL epel-release wget perl initscripts initscripts linux-firmware grubby rsync openssh-server openssh-clients git
RUN rpm -i https://linuxsoft.cern.ch/cern/centos/7/updates/x86_64/Packages/kernel-3.10.0-957.27.2.el7.x86_64.rpm
RUN rpm -i https://linuxsoft.cern.ch/cern/centos/7/updates/x86_64/Packages/kernel-devel-3.10.0-957.27.2.el7.x86_64.rpm
RUN yum remove -y kernel-headers.x86_64 && rpm -i https://linuxsoft.cern.ch/cern/centos/7/updates/x86_64/Packages/kernel-headers-3.10.0-957.27.2.el7.x86_64.rpm
RUN wget http://download.virtualbox.org/virtualbox/rpm/rhel/virtualbox.repo && mv virtualbox.repo /etc/yum.repos.d/ && yum install -y VirtualBox-6.1.x86_64 gcc make
RUN /sbin/vboxconfig
RUN wget https://releases.hashicorp.com/vagrant/2.2.18/vagrant_2.2.18_x86_64.rpm vagrant.rpm; rpm -i vagrant_2.2.18_x86_64.rpm && vagrant plugin install vagrant-vbguest --plugin-version 0.21

ENTRYPOINT "/bin/bash"
